<?php

namespace addons\shopro\job;

use addons\shopro\model\ActivityGroupon;
use addons\shopro\model\User;
use addons\shopro\model\UserWalletLog;
use think\Db;
use think\queue\Job;


/**
 * 助力砍自动操作
 */
class ActivityBargainAutoOper extends BaseJob
{
    /**
     * 助力砍自动操作判断，助力砍结束,
     */
    public function expire(Job $job, $data){
        try {
            $bargainirg_id = $data['bargainirg_id'];

            $bargainirg = db('shopro_activity_bargainirg')->find($bargainirg_id);

            // 活动正在进行中， 走这里的说明砍价失败
//            if ($bargainirg && $bargainirg['cut_total_money'] > 0 && $bargainirg['cut_total_money'] < $bargainirg['activity_price'] &&  $bargainirg['is_back_money'] ==0) {
//
//                $userModel = new User();
//                $user = $userModel->find($bargainirg['user_id']);
//                $userWalletModel = new UserWalletLog();
//                DB::startTrans();
//                try{
//                    db('user')->where(['id'=>$bargainirg['user_id']])->setInc('money',$bargainirg['cut_total_money']);
//                    db('retail_user')->where(['uid'=>$bargainirg['user_id']])->setInc('total_income',$bargainirg['cut_total_money']);
//                    $userWalletModel->doAdd($user, $bargainirg['cut_total_money'], "zhulikan_back", $bargainirg['id'], "money", 1);
//
//                    Db::commit();
//                }catch (\Exception $e){
//                    \think\Log::write('queue-' . get_class() . '-expire' . '：执行失败，错误信息：' . $e->getMessage());
//                    DB::rollback();
//                }
//            }

            // 删除 job
            $job->delete();
        } catch (\Exception $e) {
            // 队列执行失败
            \think\Log::write('queue-' . get_class() . '-expire' . '：执行失败，错误信息：' . $e->getMessage());
        }
    }
    
}