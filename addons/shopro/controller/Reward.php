<?php

namespace addons\shopro\controller;


class Reward extends Base
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    public function _initialize()
    {
        return parent::_initialize();
    }

    /**
     * 奖池最新情况
     */
    public function latest()
    {
        $retdata = [
            'total'     => 0,
            'nickname'  => '',
            'total_wanke'  => 0,
        ];

        $month = date('Ym');
        $reward = db('retail_reward')->where(['month'=>$month])->find();

        if(!empty($reward)){
            $rewardLog = db('retail_reward_log')->where(['reward_id'=>$reward['id']])->order('id desc')->find();
            $retdata['total'] = (int)$reward['total'];

            if($rewardLog){
                $retdata['nickname'] = $rewardLog['nickname'];
                $retdata['total_wanke'] = $rewardLog['total_wanke'];
            }

        }

        $this->success('奖池信息',$retdata);
    }


}