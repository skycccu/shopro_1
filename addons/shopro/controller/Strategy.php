<?php

namespace addons\shopro\controller;

use app\admin\model\shopro\Strategy as strate;

/**
 * 攻略
 *
 */

class Strategy extends Base
{
    protected $noNeedLogin = ['strategylist','detailstrategy','banner'];
    protected $noNeedRight = ['*'];

    protected $model = null;

    /**
     * 攻略列表
     */
     public function strategylist(){
        $page = $this->request->param('page');
        $info = strate::where('deletetime', null)->order('status', 'DESC')->order('views', 'DESC')->order('id', 'ASC')->page($page)->paginate(10);

        foreach ($info as $key => &$value) {
            $value['image'] = request()->domain() .$value['image'];
        }
        $this->success('获取攻略列表',$info);
    }

    /**攻略详情 */
    public function detailstrategy(){
        $id = $this->request->param('id');
        $info = db('shopro_strategy')
            ->where('id',$id)
            ->where('deletetime','null')
            ->find();
        $info['image'] = request()->domain() .$info['image'];
        $this->success('获取攻略详情',$info);
    }

    /**
     * 攻略轮播图
     */
    public function banner(){
        $info = db('config')
            ->field('value')
            ->where('name','strategy_banner')
            ->find();
        $images = \explode(',',$info['value']);
        foreach ($images as $key => $value) {
            $res[] = request()->domain() .$value;
        }
        $this->success('获取攻略轮播图',$res);
    }
}
