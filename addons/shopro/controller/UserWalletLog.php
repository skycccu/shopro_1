<?php

namespace addons\shopro\controller;

use addons\shopro\exception\Exception;

class UserWalletLog extends Base
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];


    public function index()
    {
        $params = $this->request->get();
        $wallet_type = $params['wallet_type'] ?? 'money';
        $status = $params['status'] ?? 'all';

        if (!in_array($wallet_type, ['money', 'score'])) {
            throw new Exception('参数错误');
        }

        $this->success(($wallet_type == 'money' ? '钱包记录' : '积分记录'), \addons\shopro\model\UserWalletLog::getList($wallet_type, $status));
    }

    public function getListByType()
    {
        $params = $this->request->get();
        $wallet_type = 'money';
        $status = $params['status'] ?? 'all';

        if (!in_array($wallet_type, ['money', 'score'])) {
            throw new Exception('参数错误');
        }
        $typearr = ["recharge","pay","retail","partner"];
        if(empty($params['log_type']) || !in_array($params['log_type'],$typearr)){
            throw new Exception('log_type参数错误');
        }
       switch ($params['log_type']){
           case 'recharge':
               $type =  \addons\shopro\model\UserWalletLog::$rechargeType;
               break;
           case 'pay':
               //抢助收益
               $type =  \addons\shopro\model\UserWalletLog::$payType;
               break;
           case 'retail':
               $type =  \addons\shopro\model\UserWalletLog::$retailType;
               break;
           case 'partner':
               $type =  \addons\shopro\model\UserWalletLog::$partnerType;
               break;
       }
        $data = \addons\shopro\model\UserWalletLog::getListByType($wallet_type,$type, $status);

        $this->success(($wallet_type == 'money' ? '钱包记录' : '积分记录'), $data);
    }


}
