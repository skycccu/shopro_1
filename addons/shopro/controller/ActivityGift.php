<?php

namespace addons\shopro\controller;

use app\admin\model\shopro\activity\Gift;

/**
 * 高级礼包和旅游礼包接口
 *
 */

class ActivityGift extends Base
{
    protected $noNeedLogin = ['index','givegift','signtravel','test','getgiftorder','littledetail','businfo'];
    protected $noNeedRight = ['*'];

    protected $model = null;


    public function test(){

        $partner = db('retail_partner')->field('ys_conf,jd_conf,cs_conf,uid,zone_id')->select();
        $partner = \collection($partner)->toArray();
        foreach ($partner as $item){    
            $useridsarr[] = $item['uid'];
            $dbuser = db('retail_user_address')->field('user_id')->where('province_id or city_id or area_id or zone_id',$item['zone_id'])->select();
            $zoneres = \collection($dbuser)->toArray();
            foreach ($zoneres as $key) {
                $zone_user[] = $key['user_id'];
            }

        }

        $platform_uid = db('retail_partner')
                        ->alias('p')
                        ->field('u.pid')
                        ->join('retail_user u','p.uid=u.uid')
                        ->select();
        $platform_uid = \collection($platform_uid)->toArray();
        foreach ($platform_uid as $item){    
            $platuserid[] = $item['pid']; 

        }
        $itemuser = array_intersect($useridsarr,$platuserid);
        // var_dump($platuserid); exit;
        var_dump($itemuser); exit;
        $params = $this->request->get();
        $this->success('礼包列表', \addons\shopro\model\Giftlog::insertGiftlog($params));
    }

    /**
     * 获取所有礼包
     */
    public function index() {
        $params = $this->request->get();

        // $auth = \app\common\library\Auth::instance();
        // $auth->setAllowFields(['id', 'username', 'nickname']);
        // $data = $auth->getUserinfo();
        // $id   = $data['id'];
        $type = $this->request->param('type');
        $this->success('礼包列表', \addons\shopro\model\Gift::getActivityGift($type));
    }

    /**
     * 礼包详情
     */
    public function detail() {
        $id = $this->request->get('id');
        $is_big = '1';
        $this->success('获取礼包详情', \addons\shopro\model\Gift::getActivityGiftDetail($id,$is_big));
    }

    /**
     * 小礼包详情
     */
    public function littledetail() {
        $id     = $this->request->get('id');
        $is_big = '0'; 
        $this->success('获取礼包详情', \addons\shopro\model\Gift::getActivityGiftDetail($id,$is_big));
    }

    /**
     * 获取赠送小礼包 
     */
    public function givegift() {
        
        $auth = \app\common\library\Auth::instance();
        $auth->setAllowFields(['id', 'username', 'nickname']);
        $data = $auth->getUserinfo();
        $id   = $data['id'];
        $type = $this->request->get('type'); 
        $this->success('获取小礼包',  \addons\shopro\model\Gift::getActivityGivegift($id,$type));
    }

    /**
     * 玩家报名旅游套餐
     */
    public function signtravel(){

        $auth = \app\common\library\Auth::instance();
        $auth->setAllowFields(['id', 'username', 'nickname']);
        $data = $auth->getUserinfo();
        $id   = $data['id'];

        $info = [
            'user_id'   =>$id,
            'type'      =>'travel',
            'user_name' =>$this->request->post('user_name'),
            'phone_num' =>$this->request->post('phone_num'),
            'id_card'   =>$this->request->post('id_card'),
            'gift_give_id' => $this->request->post('gift_id'),
            'bus_info' => $this->request->post('bus_info'),
            'createtime' => time()
        ];

        if(!isset($info['user_name']) || !isset($info['bus_info'])){
            $this->error('参数缺失');
        }

        $travel_log = db('retail_userdraw_gift')
                    ->where('user_id',$id)->where('type','travel')
                    ->order('createtime','desc')
                    ->limit(1)
                    ->find();
        if($travel_log){
            if($travel_log['id_card'] != $info['id_card'] || $travel_log['user_name'] != $info['user_name']){
                $this->error('用户姓名或者身份证信息与上次不一样请重新确认');    
            }
        }

        $preg_phone='/^1[34578]\d{9}$/ims';
        if(!preg_match($preg_phone,$info['phone_num'])){
            $this->error('电话号码有误');
        }

        $this->success('信息填写成功',  \addons\shopro\model\Gift::insTravel($info));
    }

    /**
     * 获取候车点信息
     */
    public function businfo(){
        $id = $this->request->param('id');
        $info = db('retail_gift_give')->field('bus_info')->where('id',$id)->where('type','travel')->find();

        $auth = \app\common\library\Auth::instance();
        $auth->setAllowFields(['id', 'username', 'nickname']);
        $data = $auth->getUserinfo();
        $id   = $data['id'];
        $addinfo = db('retail_userdraw_gift')
                ->field('user_name,phone_num,id_card')
                ->where('user_id',$id)
                ->where('type','travel')
                ->order('createtime','desc')
                ->limit(1)
                ->find();
        if($info){
            if(empty($info['bus_info'])){
                $bus_info['bus_info'] = [];    
            }else{
                $bus_info['bus_info'] = explode('|',$info['bus_info']);
            }
            $bus_info['info'] = $addinfo;
            $this->success('候车点信息',$bus_info);
        }else{
            $this->error('没有相关候车信息');
        }
    }

    /**
     * 获取订单列表
     */
    public function getgiftorder(){
        $type    = $this->request->get('type');
        $user_id = $this->request->get('user_id');
        $this->success('获取订单信息',  \addons\shopro\model\Gift::getOrderlist($type,$user_id));
    }
}
