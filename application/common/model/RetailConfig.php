<?php

namespace app\common\model;

use think\Cache;
use think\Model;

/**
 * 分销配置数据模型
 */
class RetailConfig extends Model
{

    /**
     * 获取配置信息
     */
    public function getRecommendConfig($field = 'recommend')
    {
        $recommend = db('retail_config')->column('level,' . $field);

        return $recommend;
    }



}
