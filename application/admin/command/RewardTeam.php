<?php

/**
 * Created by PhpStorm.
 * User: Lomon
 * Date: 2020/12/03
 * 定时执行 每个月1号分配上一个月的团队销售额佣金
 * 默认 0 7 1 * * 每月1号凌晨7点
 *
 * 团队销售额佣金：
    A是玩客，A次月1号会获得团队上月销售额收入的10%，团队销售额=A的销售收入+B+B1+B2+B3.......的销售收入的总和
    A是玩主，A次月1号会获得团队上月销售额收入的20%，团队销售额=A的销售收入+B+B1+B2+B3.......的销售收入的总和
    A是玩家，A次月1号会获得团队上月销售额收入的30%，团队销售额=A的销售收入+B+B1+B2+B3.......的销售收入的总和
    销售收入:查订单表分享人
 */
namespace app\admin\command;

use addons\shopro\model\UserWalletLog;
use app\common\model\RetailConfig;
use app\common\model\RetailReward;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;

use app\common\model\RetailUser;
use think\Db;

class RewardTeam extends Command
{
    protected function configure()
    {
        $this->setName('RewardTeam')->setDescription('每个月1号分配上一个月的团队销售额佣金');
    }
    protected function execute(Input $input, Output $output)
    {
        $WalletModel = new UserWalletLog();

        //上个月1号
        $lastMonth = strtotime(date('Y-m-01',strtotime("last month")));
        //这个月1号
        $thisMonth = strtotime(date('Y-m-01'));

        try{
            //分配给直属上级X元,retail_config.under_wanjia,根据B的等级来决定给多少推荐奖励
            $retailConfigModel = new RetailConfig();
            $conf   = $retailConfigModel->getRecommendConfig("team");

            //大于粉丝的会员
            $users = db('retail_user')->where(['level'=>['gt',RetailUser::LEVEL_FENS],'status'=>RetailUser::STATUS_ENABLE])->column('uid,level,total_income');

            $type = "sale_reward_team";
            foreach ($users as $k=>$u){
                //判断此用户是否已经分过上个月的团队收入了
                $w = ['user_id'=>$k,
                    'type'=>$type,
                    'wallet_type'=>'money',
                    'create_time'=>['gt',$thisMonth]
                ];
                $log = db('user_wallet_log')->where($w)->find;

                if($log){
                    continue;
                }

                $u['id'] = $k;
                //统计团队成员
                $teamIds = model('retail_user')->teamPersonList($u,true);

                //统计团队销售收入总额,wallet_log.type=sale_reward
                $team_w = [
                    'user_id'=>['in',$teamIds],
                    'create_time'=>['between',"{$lastMonth},{$thisMonth}"]
                ];
                $wallet_sum = db('user_wallet_log')->where($team_w)
                    ->sum("wallet");

                //开始团队销售额佣金
                if($wallet_sum && !empty($conf[$u['level']])){
                    $commission = $wallet_sum * $conf[$u['level']];

                    Db::startTrans();
                    try{

                        db('user')->where(['id'=>$k])->setInc('money',$commission);

                        $userWallet = clone $WalletModel;
                        $userWallet->doAdd($u, $commission, $type, 0, "money", 1);

                        Db::commit();
                    }catch (\Exception $e){
                        Db::rollback();
                        \think\log::error("用户:{$u['id']}的团队分配任务失败:".$e->getMessage());
                    }
                }
            }
        }catch (\Exception $e){
            \think\log::error("团队分成分配任务失败:".$e->getMessage());
            exit;
        }

        echo "分配成功";exit;
    }
}