<?php

/**
 * User: Yzw
 * Date: 2020/12/10
 * 定时执行 统计发放合伙人的收益
 * 默认 0/1 0 0 * * 每日凌晨01分
 *
 * 每个月1号执行脚本,更新玩家拥有的权益
 */
namespace app\admin\command;

use app\common\model\MoneyLog;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use think\Db;
use addons\shopro\model\User;
use addons\shopro\model\UserWalletLog;

class Partnermoney extends Command
{
    protected function configure()
    {
        $this->setName('Partnermoney')->setDescription('发放合伙人收益');
    }
    protected function execute(Input $input, Output $output)
    {    
        
        $type = '1'; /**1年度奖金发放  2季度奖金发放  3城市奖金发放*/

        switch ($type) {
            case '1':
                $begin_time   = strtotime(date('Y-01-01', strtotime('-1 year')));
                $end_time = \strtotime(date('Y-12-31 23:59:59'),time());
                $where = ['createtime'=>['between',"$begin_time,$end_time"]];
                $field = 'SUM(ys_income) as ys_income,SUM(jd_income) as jd_income,SUM(cs_income) as cs_income,uid';
                break;
            case '2':
                $begin_time   = strtotime(date("Y-m-d 00:00:00", strtotime("-3 month")));
                $end_time = strtotime(date("Y-m-d 23:59:59", strtotime("-1 day")));
                $where = ['createtime'=>['between',"$begin_time,$end_time"]];
                $field = 'SUM(ys_income) as ys_income,SUM(jd_income) as jd_income,SUM(cs_income) as cs_income,uid';
                break;
            case '3':
                $begin_time   = strtotime(date("Y-m-d 00:00:00", strtotime("-1 month")));
                $end_time = strtotime(date("Y-m-d 23:59:59", strtotime("-1 day")));
                $where = ['createtime'=>['between',"$begin_time,$end_time"]];
                $field = 'SUM(ys_income) as ys_income,SUM(jd_income) as jd_income,SUM(cs_income) as cs_income,uid';
                break;
            default:
                # code...
                break;
        }
        $allreword = db('retail_partner_reword_log')
                    ->field($field)
                    ->where($where)
                    ->group('uid')
                    ->select();

        $allreword = \collection($allreword)->toArray();
        //1.获取到所有的合伙人
        $partner = db('retail_partner')->field('uid')->select();
        $partner = \collection($partner)->toArray();

        try{
            Db::startTrans();
            try{
                foreach ($partner as $item){    
                    foreach ($allreword as $key => $value) {
                        if($value['uid'] == $item['uid']){
                            $info['ys_income'] = $value['ys_income'];
                            $info['jd_income'] = $value['jd_income'];
                            $info['cs_income'] = $value['cs_income'];
                            $info['uid']  = $item['uid'];
                            $this->insertData($info,$type);
                        }
                    }
                }
                Db::commit();
            }catch (\Exception $e){
                Db::rollback();
                \think\log::error("更新每日利润:".$e->getMessage());
                exit;
            }

        }catch (\Exception $e){
            \think\log::error("更新每日利润:".$e->getMessage());
            exit;
        }

        echo "更新成功";exit;
    }

    /**插入数据 */
    
    protected function insertData(array $info,$type){
        //$type = '1'; /**1年度奖金发放  2季度奖金发放  3城市奖金发放*/
        switch ($type) {
            case '1':
                $info['code'] = 'partner_reward_pfadmin';
                $data = [
                    'ys_income'=>$info['ys_income'],
                ];
                $money = $info['ys_income'];
                $type_info = '年度奖金收益记录';
                break;
            case '2':
                $info['code'] = 'partner_reward_pfadmin';
                $data = [
                    'jd_income'=>$info['jd_income'],
                ];
                $money = $info['jd_income'];
                $type_info = '季度奖金收益记录';
                break;
            case '3':
                $info['code'] = 'partner_reward_cs';
                $data = [
                    'cs_income'=>$info['cs_income'],
                ];
                $money = $info['jd_income'];
                $type_info = '城市收益记录';
                break;
            default:
                # code...
                break;
        }
        /**更新合伙人的收益记录 */
        $res = db('retail_partner')->where('uid',$info['uid'])->update($data);
        /**更新用户钱包日志 */
        $user = db('user')->field('money')->where('id',$info['uid'])->find();
        $sql = "update fa_user set money = money + {$money} where id ={$info['uid']}";
        Db::execute($sql);

        $userModel = new User();
        $user = $userModel->find($info['uid']);

        $WalletModel = new UserWalletLog();
        $WalletModel->doAdd($user, $money, $info['code'], '0', "money", 1);

        //写入日志
        MoneyLog::create(['user_id' => $info['uid'], 'money' => $money, 'before' => $user['money'] , 'after' => $user['money'] + $money, 'memo' => $type_info]);
    }
}