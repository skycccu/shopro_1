<?php

/**
 * User: Yzw
 * Date: 2020/12/10
 * 定时执行 每个月1号更新一下玩家拥有的领取小礼包的次数
 * 默认 0 1 1 * * 每月1号凌晨1点
 *
 * 每个月1号执行脚本,更新玩家拥有的权益
 */
namespace app\admin\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use think\Db;

class Updategift extends Command
{
    protected function configure()
    {
        $this->setName('Updategift')->setDescription('每个月1号更新玩家拥有的权益');
    }
    protected function execute(Input $input, Output $output)
    {
        try{
            //1.查询有效期内有权益的所以用户
            $end_time   = time();

            // $where = ['buy_time'=>['gt',"$end_time"]];
            $res = db('retail_userbuy_gift')->field('gift_id,type,user_id')->where('buy_time','>',$end_time)->select();
            $res = \collection($res)->toArray();
            Db::startTrans();
            try{
                foreach ($res as $item){    
                    $useridsarr[] = $item['user_id'];
                    $gift_info = db('retail_gift')
                                ->field('live_count')
                                ->where('gift_id',$item['gift_id'])
                                ->where('type',$item['type']);
                    $info = [
                        'receive_num'=>$gift_info['live_count']
                    ];
                    $result = db('retail_userbuy_gift')
                            ->where('gift_id',$item['gift_id'])
                            ->where('user_id',$item['user_id'])
                            ->where('type',$item['type'])
                            ->where('buy_time','>',$end_time)
                            ->update($info);
                    unset($info);
                }
                Db::commit();
            }catch (\Exception $e){
                Db::rollback();
                \think\log::error("更新用户每月权益:".$e->getMessage());
                exit;
            }

        }catch (\Exception $e){
            \think\log::error("更新用户每月权益:".$e->getMessage());
            exit;
        }

        echo "更新成功";exit;
    }
}