<?php

namespace app\admin\controller\finance;

use app\common\controller\Backend;
use app\common\library\Auth;

/**
 * 合伙人列表
 *
 * @icon fa fa-user
 */
class Partner extends Backend
{

    protected $relationSearch = true;
    protected $searchFields = 'id';

    protected $areaModel = null;

    /**
     * @var \app\admin\model\User
     */
    protected $model = null;

    public function _initialize()
    {   
        $groupdata = [1=>'共创合伙人',2=>'城市合伙人',3=>'区县合伙人',4=>'乡镇合伙人'];
        parent::_initialize();
        $this->model     =  new \app\admin\model\retail\Partner;
        $this->areaModel =  model('Cnarea2019');
        $this->view->assign('groupdata', $groupdata);
    }

    /**
     * 查看
     */
    public function index()
    {

        //设置过滤方法
        $this->request->filter(['strip_tags', 'trim']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();

            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $list = $this->model
                ->where($where)
                ->paginate($limit);

            $result = array("total" => $list->total(), "rows" => $list->items());

            return json($result);
        }
        return $this->view->fetch();
    }


}
