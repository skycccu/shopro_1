define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'retail/partnerconf/index',
                    edit_url: 'retail/partnerconf/edit',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'type', title: __('合伙人级别'),formatter:Controller.api.formatter.level,searchable:false},
                        {field: 'ys_conf', title: __('原始股'),operate: false},
                        {field: 'jd_conf', title: __('平台季度分红'),operate: false},
                        {field: 'cs_conf', title: __('城市加权分红'),operate: false},
                        {field: 'join_num', title: __('加盟价格元'),operate: false},
                        {field: 'tj_conf', title: __('推荐奖励'),operate: false},
                        {field: 'jt_conf', title: __('间推积分'),operate: false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            formatter: {
                level: function (value, row, index) {
                    var title = '';
                    if(value == 2){
                        title = '城市合伙人';
                    }else if(value == 3){
                        title = '区县合伙人';
                    }else if(value == 4){
                        title = '乡镇合伙人';
                    }else{
                        title = '共创合伙人';
                    }
                    return title;
                },
            },
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});