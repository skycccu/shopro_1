define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'retail/user/index',
                    edit_url: 'retail/user/edit',
                    table: 'user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'uid',
                columns: [
                    [
                        {field: 'uid', title: __('Id'), sortable: true},
                        {field: 'pid', title: __('上级用户id'), sortable: true},
                        {field: 'fens_total', title: __('团队人数'), sortable: true},
                        {field: 'user.nickname', title: __('Nickname'), operate: 'LIKE'},
                        {field: 'user.username', title: __('Username'), operate: 'LIKE'},
                        {field: 'user.money', title: __('账户金额'), operate: false},
                        {field: 'level', title: __('Level'), formatter:Controller.api.formatter.level,searchable:false},
                        {field: 'user.avatar', title: __('Avatar'), events: Table.api.events.image, formatter: Table.api.formatter.image, operate: false},
                        {field: 'user.gender', title: __('Gender'),  formatter:Controller.api.formatter.gender,searchList: {1: __('Male'), 0: __('Female')},searchable:false},
                        {field: 'status', title: __('Status'), formatter: Table.api.formatter.status, searchList: {1: __('Normal'), 2:"黑名单"}},
                        {field: 'user.createtime', title: __('创建时间'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'create_time', title: __('Jointime'), formatter: Table.api.formatter.datetime, operate: 'RANGE', addclass: 'datetimerange', sortable: true},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
                level: function (value, row, index) {
                    var title = '';
                    if(value == 2){
                        title = '玩客';
                    }else if(value == 3){
                        title = '玩主';
                    }else if(value == 4){
                        title = '玩家';
                    }else{
                        title = '粉丝';
                    }
                    return title;
                },
                gender: function (value, row, index) {
                    var content = '女';
                    if(value == 1){
                        content = "男";
                    }
                    return content;
                },
            },
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});