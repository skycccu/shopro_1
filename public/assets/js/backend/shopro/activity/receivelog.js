define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shopro/activity/receivelog/index',
                    // del_url: 'shopro/activity/usergift/del',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'gift_give_id', title: __('赠送小礼包id')},
                        {field: 'type', title: __('类型'), searchList: {gift: __('高级礼包配置'), travel: __('旅游礼包配置')}},
                        {field: 'status', title: __('状态'), searchList: {1: __('已领取'), 0: __('已取消')}},
                        {field: 'user_id', title: __('用户id'),operate: false},
                        {field: 'nickname', title: __('用户昵称'),operate: 'like'},
                        {field: 'user_name', title: __('用户姓名'),  operate: 'like'},
                        {field: 'phone_num', title: __('电话号码'),  operate: 'like'},
                        {field: 'id_card', title: __('身份证id'),operate: false},
                        {field: 'bus_info', title: __('候车点'),operate: 'like'},
                        {field: 'createtime', title: __('领取时间'),  operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
            },
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});