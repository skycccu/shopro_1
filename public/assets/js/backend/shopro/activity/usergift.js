define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shopro/activity/usergift/index',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'gift_id', title: __('Gift_id'),operate: false},
                        {field: 'type', title: __('Type'), searchList: {gift: __('Gift'), travel: __('Travel')}},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'nickname', title: __('用户昵称'),operate: false},
                        {field: 'is_big', title: __('是否为大礼包'),searchList: {1: __('大礼包'), 0: __('小礼包')}},
                        // {field: 'user_name', title: __('User_name'),  operate: 'like'},
                        // {field: 'phone_num', title: __('Phone_num'),  operate: 'like'},
                        // {field: 'id_card', title: __('Id_card')},
                        {field: 'createtime', title: __('购买时间'),  operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
            },
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});