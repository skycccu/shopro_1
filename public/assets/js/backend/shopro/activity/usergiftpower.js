define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shopro/activity/usergiftpower/index',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'), sortable: true},
                        {field: 'type', title: __('Type'), searchList: {gift: __('Gift'), travel: __('Travel')}},
                        {field: 'user_id', title: __('用户id')},
                        {field: 'nickname', title: __('用户昵称'),operate: false},
                        {field: 'receive_num', title: __('当月免领数'),operate: false},
                        // {field: 'user_name', title: __('用户姓名'),  operate: 'like'},
                        // {field: 'phone_num', title: __('电话号码'),  operate: 'like'},
                        // {field: 'id_card', title: __('身份证id'),operate: false},
                        {field: 'buy_time', title: __('权益更新时间'),  operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'createtime', title: __('购买时间'),  operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
            },
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});