define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'shopro/activity/bargaininfo/index',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('砍价活动id'), sortable: true},
                        {field: 'goods_name', title: __('砍价名称'),operate: false},
                        {field: 'bargain_count', title: __('砍价人数'),operate: false},
                        {field: 'activity_price', title: __('砍价总价格'),operate: false},
                        {field: 'cut_total_money', title: __('已经被砍价格'),operate: false},
                        {field: 'user_id', title: __('砍价用户id')},
                        {field: 'nickname', title: __('用户昵称'),operate: false},
                        {field: 'createtime', title: __('活动时间'),  operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
            },
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});