define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'finance/bonushare/index',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'create_time', title: __('创建时间'),  operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'user_id', title: __('用户id')},
                        {field: 'nickname', title: __('用户昵称')},
                        {field: 'money', title: __('分配金额'),operate: false},
                        // {field: 'platform_profit', title: __('平台收益'),operate: false},
                        // {field: 'parent_level', title: __('上级会员扽等级'),searchList: {0: __('普通用户'), 1: __('粉丝'),2: __('玩客'),3: __('玩主'),4: __('玩家')}},
                        // {field: 'user_name', title: __('User_name'),  operate: 'like'},
                        // {field: 'phone_num', title: __('Phone_num'),  operate: 'like'},
                        // {field: 'id_card', title: __('Id_card')},
    
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
            },
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});