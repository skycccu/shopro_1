define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'finance/orderdayrpt/index',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortName: 'date',
                searchFormVisible: true,
                searchFormTemplate: 'customformtpl',
                columns: [
                    [
                        {field: 'date', title: '日期', sortable: true},
                        {field: 'agent_name', title: '供应商'},
                        {field: 'activity_type', title: '类型'},
                        {field: 'order_cnt', title: '订单数量'},
                        {field: 'order_person_cnt', title: '订单人数'},
                        {field: 'price_cnt', title: '金额'},
                        {field: 'cost_cnt', title: '成本'},
                        {field: 'profit', title: '利润'},
                        {field: 'commission_cnt', title: '佣金分成'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
            },
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});