define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'finance/orderstatusrpt/index',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                sortName: 'date',
                searchFormVisible: true,
                searchFormTemplate: 'customformtpl',
                columns: [
                    [
                        {field: 'date', title: '日期', sortable: true},
                        {field: 'cancel', title: '已取消数量'},
                        {field: 'close', title: '已关闭数量'},
                        {field: 'nopay', title: '未支付数量'},
                        {field: 'payed', title: '已支付数量'},
                        {field: 'finish', title: '已完成数量'},
                        {field: 'payed_amount', title: '已支付总金额'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        api: {
            formatter: {
            },
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});